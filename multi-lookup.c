#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include "util.h"

#define BUFFER_SIZE 20

/* 
    Compile: gcc multi-lookup.c util.c -pthread
    Run: ./a.out requesterLog.txt resolverLog.txt dataList.txt
*/

struct masterStruct {
    pthread_mutex_t rIncrementLock;
    pthread_mutex_t wIncrementLock;
    pthread_mutex_t readerLock;
    pthread_mutex_t writerLock;
    pthread_mutex_t fileToProcessLock;
    pthread_mutex_t oFileLock;

    pthread_cond_t notEmpty;
    pthread_cond_t notFull;

    FILE *oFile;

    int totalRead;
    int totalWrite;

    int writeIndex;
    int readIndex;

    int numOfFilesToProcess;
    int numOfWriteThreads;
    int numOfReadThreads;

    char* fileToProcess[100]; 
    char* buffer[BUFFER_SIZE];
};

//Thread function that writes to buffer. 
void* writer(void * args)
{
    struct masterStruct *mStruct = (struct masterStruct*) args;
    FILE* fPointer;
    pid_t id = pthread_self();
    printf("Thread ID: %i\n", id);
   
    for(;;)
    {    
        pthread_mutex_lock(&mStruct->fileToProcessLock);
        mStruct->numOfFilesToProcess--;
        int cFileToProcess = mStruct->numOfFilesToProcess;
        pthread_mutex_unlock(&mStruct->fileToProcessLock);


        if(cFileToProcess < 0)
        {
            break;
        }

        char fileName[strlen(mStruct->fileToProcess[cFileToProcess])];

        snprintf(fileName, sizeof(fileName), "%s", mStruct->fileToProcess[cFileToProcess]); 
        //FREE DAT SHIT
        free(mStruct->fileToProcess[cFileToProcess]);

        printf("File we are working on is : %s\n", fileName);
        fPointer = fopen(fileName, "r");

        //Check if file exists
        if(fPointer == NULL)
        {
            printf("File Not Found: %s\n", fileName);
            continue;
        }

        while(!feof(fPointer))
        {
            char temp[1025];
            //Copy this files line to a temp
            fgets(temp, 1025, fPointer);

            pthread_mutex_lock(&mStruct->writerLock);
            //Check if the Buffer is Full oops
            while(mStruct->writeIndex == mStruct->readIndex && (mStruct->totalWrite - mStruct->totalRead) >= BUFFER_SIZE)
            {
                printf("Inside Writer Wait | wIndex : %i | rIndex : %i | totalWrites : %i | totalReads : %i\n", mStruct->writeIndex, mStruct->readIndex, mStruct->totalWrite, mStruct->totalRead);;
                pthread_cond_wait(&mStruct->notFull, &mStruct->writerLock);
                printf("Done Writer Waiting\n");
            }

            int currentIndex = mStruct->writeIndex;

            if(currentIndex >= BUFFER_SIZE - 1)
            {
                mStruct->writeIndex = 0;
            } else 
            {
                mStruct->writeIndex++;
            }
            
            mStruct->totalWrite++;

            // Allocate Memory for the string then copy the string to it then set the pointer in the buffer to that memory FML.
            strcpy(mStruct->buffer[currentIndex], temp);
            pthread_mutex_unlock(&mStruct->writerLock);

            pthread_cond_signal(&mStruct->notEmpty);
            
            printf("Write at %i\n", mStruct->writeIndex);            

        }
        fclose(fPointer);
    }
    //Rename the lock
    pthread_mutex_lock(&mStruct->wIncrementLock);
    mStruct->numOfWriteThreads--;
    printf("Writer Terminated: Living Threads = %i\n", mStruct->numOfWriteThreads);
    pthread_mutex_unlock(&mStruct->wIncrementLock);
    
    pthread_cond_signal(&mStruct->notEmpty);

    pthread_exit(0);
}
//Thread function that Reads from buffer and Makes IP addresses
void* reader(void* args)
{
    struct masterStruct *mStruct = (struct masterStruct*) args;

    FILE* fPointer = mStruct->oFile;
    
    for(;;)
    {

        char dnsTemp[1025];
        char tempIP[1025];
        char* finalEntry;
        pthread_mutex_lock(&mStruct->readerLock);
        if(mStruct->totalRead >= mStruct->totalWrite && mStruct->numOfWriteThreads <= 0 && mStruct->totalWrite > 0)
        {
            mStruct->numOfReadThreads--;
            pthread_mutex_unlock(&mStruct->readerLock);
            pthread_cond_signal(&mStruct->notEmpty);

            printf("Reader Terminated: Number of living threads %i\n", mStruct->numOfReadThreads);
        
            break;
        }

        //Check if the buffer is Empty
        while(mStruct->readIndex == mStruct->writeIndex && mStruct->totalWrite <= mStruct->totalRead && mStruct->totalRead != 0)
        {
            if (mStruct->numOfWriteThreads == 0)
            {
                pthread_mutex_unlock(&mStruct->readerLock);
                pthread_cond_signal(&mStruct->notEmpty);
                break;
            }
            printf("Inside READER Wait | wIndex : %i | rIndex : %i | totalWrites : %i | totalReads : %i | Number of Write Threads %i | Number of Read Threads %i\n", mStruct->writeIndex, mStruct->readIndex, mStruct->totalWrite, mStruct->totalRead, mStruct->numOfWriteThreads, mStruct->numOfReadThreads);
            pthread_cond_wait(&mStruct->notEmpty, &mStruct->readerLock);
            printf("Reader Completed Wait\n");
        }

        //Lock updating of index

        printf("Read at %i | Total Read : %i | Total Write : %i\n", mStruct->readIndex, mStruct->totalRead, mStruct->totalWrite);
        
        int currentIndex = mStruct->readIndex;

        if(currentIndex >= BUFFER_SIZE - 1)
        {
            mStruct->readIndex = 0;
        } else {
            mStruct->readIndex++;
        }
        
        mStruct->totalRead++;


        // Pull DNS from file structure
        snprintf(dnsTemp, strlen(mStruct->buffer[currentIndex]), "%s", mStruct->buffer[currentIndex]);
        
        //Signal I've done work
        pthread_cond_signal(&mStruct->notFull); 

        pthread_mutex_unlock(&mStruct->readerLock);
        
        //Find IP address
        int dnsFound = dnslookup(dnsTemp, tempIP, 1025);
        if(dnsFound == UTIL_FAILURE)
        {
            strcpy(tempIP, "");
        }

        printf("DNS Entry : %s\n", dnsTemp);
        
        //Lock up the file so no one else can use it. 
        pthread_mutex_lock(&mStruct->oFileLock);
        fprintf(fPointer, "%s,%s\n", dnsTemp, tempIP);  
        pthread_mutex_unlock(&mStruct->oFileLock);
    }
    // How to free finalEntry
    pthread_exit(0);
}


int main(int argc, char* argv[])
{
    time_t start, end;
    time(&start);
    
    if(argc < 6){
        printf("Dats way too few arguments requires 5!\n");
        return -1;
    } else if(argc > 6)
    {
        printf("Dats way too many arguments requires 5!\n");
        return -1;
    }


    int writerThreadCount = atoi(argv[1]);
    int readerThreadCount = atoi(argv[2]);

    // Check if Thread counts are valid. 
    if(writerThreadCount > 5)
    {
        printf("Sorry too many Requester Threads. Maximum of 5\n");
        exit(0);
    }

    if(readerThreadCount > 10)
    {
        printf("Sorry too many Resolver Threads. Maximum of 10\n");
        exit(0);
    }
    // Check if files exist
    FILE* testFile;

    testFile = fopen(argv[5], "r");
    if(testFile)
    {
        fclose(testFile);
    } else
    {
        printf("Invalid Data File\n");
        exit(0);
    }

    testFile = fopen(argv[4], "r");
    if(testFile)
    {
        fclose(testFile);
    } else
    {
        printf("Invalid Resolver Log \n");
        exit(0);
    }

    testFile = fopen(argv[3], "r");
    if(testFile)
    {
        fclose(testFile);
    } else
    {
        printf("Invalid Requester Log\n");
        exit(0);
    }

    int fileCounter = 0;
    struct masterStruct mStruct;
    // Get the names of all the files from Data List
    FILE *fPointer;
    fPointer = fopen(argv[5], "r");

    while(!feof(fPointer))
    {
        char temp[100];
        char fileExtension[] = "./input/";

        fgets(temp, 100, fPointer);

        if(strlen(temp) <= 1)
        {
            break;
        }
        strcat(fileExtension, temp);
        char* fileNameBuffer = (char*)malloc(strlen(fileExtension)*sizeof(char));
        strcpy(fileNameBuffer, fileExtension);
        mStruct.fileToProcess[fileCounter] = fileNameBuffer;
        fileCounter++;
    } 


    for(int i = 0; i < BUFFER_SIZE; i++)
    {
        mStruct.buffer[i] = (char*)malloc(1025);
    }

    fclose(fPointer);

    // Clear the file if not empty then append to it. 
    FILE* oFile;
    oFile = fopen("outputFile.txt", "w");
    fclose(oFile);
    oFile = fopen("outputFile.txt", "a");

    printf("Number of writer Threads: %i\n", writerThreadCount);
    printf("Number of reader Threads: %i\n", readerThreadCount);

    mStruct.oFile = oFile;
    mStruct.totalRead = 0;
    mStruct.totalWrite = 0;
    mStruct.numOfFilesToProcess = fileCounter;
    mStruct.readIndex = 0;
    mStruct.writeIndex = 0;
    mStruct.numOfReadThreads = readerThreadCount;
    mStruct.numOfWriteThreads = writerThreadCount;
    
    pthread_t tid[readerThreadCount + writerThreadCount];


    pthread_attr_t attr;
    pthread_attr_init(&attr);

    pthread_cond_init(&mStruct.notEmpty, (void*)&attr);
    pthread_cond_init(&mStruct.notFull, (void*)&attr);

    pthread_mutex_init(&mStruct.rIncrementLock, (void*)&attr);
    pthread_mutex_init(&mStruct.wIncrementLock, (void*)&attr);
    pthread_mutex_init(&mStruct.readerLock, (void*)&attr);
    pthread_mutex_init(&mStruct.writerLock, (void*)&attr);
    pthread_mutex_init(&mStruct.fileToProcessLock, (void*)&attr);
    pthread_mutex_init(&mStruct.oFileLock, (void*)&attr);

    for(int i = 0; i < (readerThreadCount + writerThreadCount); i++)
    {

        if(i < writerThreadCount)
        {
            printf("Created Writer Thread %i\n", i);
            pthread_create(&tid[i], &attr, writer, &mStruct);
        } else {
            printf("Created Reader Thread %i\n", i);
            pthread_create(&tid[i], &attr, reader, &mStruct);
        }
    }


    for (int i = 0; i < (readerThreadCount + writerThreadCount - 1); i++)
    {
        pthread_join(tid[i], NULL);
    }

    fclose(mStruct.oFile);
    
    //FREE IT ALL!! 
    for(int i = 0; i < BUFFER_SIZE; i++)
    {
        free(mStruct.buffer[i]);
    }

    time(&end);
    double totalTime = end - start;
    printf("Done in %.2f seconds\n", totalTime), totalTime;

}